<p>This example goes through the complete form process with just one input type (email) so that you get your form created and tested on the most simplest of terms. After that you can build the form by adding form elements one at a time, testing to make sure they work.</p>

<p>For a live sample see: <a href="http://rerickso.w3.uvm.edu/projects/form/" target="_blank">http://rerickso.w3.uvm.edu/projects/form/</a></p>

<p>It is important for you to read the comments and study the code to make sure you understand what is going on. Part of the learning process is to understand "How" to build the form. You will notice in the first commit my form has nothing in it really and is just a template to get ready. However I want to make sure that that template works before moving on. the second commit simply sets up the html for the form itself. For each commit I make sure I have that step working before I move on. It is a building process where at the end of each step it is functional. In each case when I make a mistake the amount of code I have to look through is just the small set that I have added.</p>

<p>You may wonder why I have so many blank lines in my form and that is so that the line numbers will match up from beginning to end. On each commit I fill in the blanks so to say. I hope it helps you to see where the changes are made each time.</p>

<p>One word of caution. If you copy and paste you will learn nothing. I suggest you read and type so at least the code goes into your brain and out your fingers.</p>