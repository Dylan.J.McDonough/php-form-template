<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <p>Extra example source code. Note that you need to put these sections into your form.</p>
<?php
// ####################    Adding a text area      #############################

// Initialize: SECTION 1c.
$comments="";

$commentsERROR = false;

// Sanitize: SECTION 2c.
$comments = htmlentities($_POST["txtComments"], ENT_QUOTES, "UTF-8");
$dataRecord[] = $comments;

// Error check: SECTION 2d.
// Note that this if statments mean the comments are not required 
if ($comments != "") { 
    if (!verifyAlphaNum($comments)) {
        $errorMsg[] = "Your comments appear to have extra characters that are not allowed.";
        $commentsERROR = true;
    }
}
?>

<fieldset class="textarea">					
    <label  class="required"for="txtComments">Comments</label>
    <textarea <?php if ($commentsERROR) print 'class="mistake"'; ?>
              id="txtComments" 
              name="txtComments" 
              onfocus="this.select()" 
              style="width: 25em; height: 4em;" 
              tabindex="200"><?php print $comments; ?></textarea>
              <!-- NOTE: no blank spaces inside the text area, be sure to close 
                         the text area directly
              
                         CSS shown inline for your convenience -->
</fieldset>

<?php
// ####################    Adding radio buttons      ###########################

// Initialize: SECTION 1c.
$gender="Female";

$genderERROR = false;

// Sanitize: SECTION 2c.
$gender = htmlentities($_POST["radGender"], ENT_QUOTES, "UTF-8");
$dataRecord[] = $gender;

// Error check: SECTION 2d.
// none if you set a default value.
if($gender != "Male" OR $gender != "Female"){
    $errorMsg[] = "Please choose a gender";
    $genderERROR = true;
}
?>
        
<fieldset class="radio <?php if ($genderERROR) print ' mistake'; ?>">
    <legend>What is your gender?</legend>
    <label><input <?php if ($gender == "Male") print 'checked '; ?>                   
                  id="radGenderMale" 
                  name="radGender" 
                  tabindex="330"
                  type="radio"
                  value="Male">Male</label>
    
    <label><input  <?php if ($gender == "Female") print 'checked '; ?>
                  id="radGenderFemale" 
                  name="radGender" 
                  tabindex="340"
                  type="radio"
                  value="Female">Female</label>
</fieldset>

<?php
// ####################    Adding check boxes      #############################

// Initialize: SECTION 1c.
$hiking = true;    // checked
$kayaking = false; // not checked

$activityERROR = false;
$totalChecked = 0;

//Sanitize: SECTION 2c.
// NOTE If a check box is not checked it is not sent in the POST array.
if (isset($_POST["chkHiking"])) {
    $hiking = true;
    $totalChecked++;
} else {
    $hiking = false;
}
$dataRecord[] = $hiking; 
/* the above saves true (a number 1 in yoru sv file) or false (empty) 
 * you could save the value of the check box

if (isset($_POST["chkHiking"])) {
    $hiking = true;
    $dataRecord[] = htmlentities($_POST["chkHiking"], ENT_QUOTES, "UTF-8");
    $totalChecked++; // count how many are checked if you need to
} else {
    $hiking = false;
    $dataRecord[] = ""; 
}
*/

if (isset($_POST["chkKayaking"])) {
    $kayaking = true;
    $totalChecked++;
} else {
    $kayaking = false;
}
$dataRecord[] = $kayaking;

// Error check: SECTION 2d.
// may not need to check for any
if($totalChecked < 1){
    $errorMsg[] = "Please choose at least one activity";
    $activityERROR = true;
}
?>
<fieldset class="checkbox <?php if ($activityERROR) print ' mistake'; ?>">
    <legend>Do you like (choose at least one and check all that apply):</legend>
    <label><input <?php if ($hiking) print " checked "; ?>
                  id="chkHiking" 
                  name="chkHiking" 
                  tabindex="420"
                  type="checkbox"
                  value="Hiking"> Hiking</label>

    <label><input <?php if ($kayaking)  print " checked "; ?>
                  id="chkKayaking" 
                  name="chkKayaking" 
                  tabindex="430"
                  type="checkbox"
                  value="Kayaking"> Kayaking</label>
</fieldset>

<?php
// ####################    Adding a list box       #############################

//Initialize: SECTION 1c.
$mountain = "Camels Hump";    // pick the option

$mountainError = false;

//Sanitize: SECTION 2c.
$mountain = htmlentities($_POST["lstMountains"],ENT_QUOTES,"UTF-8");
$dataRecord[] = $mountain;

//Error check: SECTION 2d.
// none if you set a default value. here i am just checking if they picked
// one. You could check to see if mountain is == to one of the ones you
// have, similar to readio buttons
if($mountain == ""){
    $errorMsg[] = "Please choose a favorite mountain";
    $mountainError = true;
}
?>
        
<fieldset  class="listbox <?php if ($mountainERROR) print ' mistake'; ?>">	
    <label for="lstMountains">Favorite Mountain</label>
    <select id="lstMountains" 
            name="lstMountains" 
            tabindex="520" >
        <option <?php if($mountain=="HayStack Mountain") print " selected "; ?>
            value="HayStack Mountain">HayStack Mountain</option>
        
        <option <?php if($mountain=="Camels Hump") print " selected "; ?>
            value="Camels Hump">Camels Hump</option>
        
        <option <?php if($mountain=="Laraway Mountain") print " selected "; ?>
            value="Laraway Mountain">Laraway Mountain</option>
    </select>
</fieldset>
    </body>
</html>